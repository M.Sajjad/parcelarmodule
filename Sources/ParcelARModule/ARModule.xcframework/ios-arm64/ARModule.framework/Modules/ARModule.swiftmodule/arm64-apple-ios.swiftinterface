// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3 (swiftlang-1200.0.29.2 clang-1200.0.30.1)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name ARModule
import ARKit
@_exported import ARModule
import Accelerate
import Alamofire
import CoreML
import Foundation
import MBProgressHUD
import SceneKit
import Swift
import SystemConfiguration
import UIKit
import Vision
public enum ScaleState : CoreGraphics.CGFloat {
  case normal
  case up
  case down
  public typealias RawValue = CoreGraphics.CGFloat
  public init?(rawValue: CoreGraphics.CGFloat)
  public var rawValue: CoreGraphics.CGFloat {
    get
  }
}
public enum TouchState : Swift.String {
  case began
  case cancelled
  case ended
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@objc @_inheritsConvenienceInitializers @IBDesignable public class AnimatedRoundButton : UIKit.UIView {
  @objc @IBInspectable public var paddingPercent: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  public var handlerButtonTouch: ((ARModule.TouchState) -> Swift.Void)?
  @objc @IBInspectable public var animationDuration: Foundation.TimeInterval {
    @objc get
    @objc set
  }
  public var animationDurationForStates: [ARModule.ScaleState : Foundation.TimeInterval]
  public var scaleFactors: [ARModule.ScaleState : CoreGraphics.CGFloat]
  public var backgroundColorsForStates: [ARModule.ScaleState : UIKit.UIColor]
  @objc @IBInspectable public var contentColor: UIKit.UIColor {
    @objc get
    @objc set
  }
  public var contentColorsForStates: [ARModule.ScaleState : UIKit.UIColor] {
    get
    set
  }
  public var safePercent: Swift.Int {
    get
    set
  }
  @objc override dynamic public func awakeFromNib()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  public var lineWidth: CoreGraphics.CGFloat {
    get
    set
  }
  public func setProgress(to progressConstant: Swift.Double, withAnimation: Swift.Bool)
  public func removeProgress()
  @objc deinit
}
extension SCNNode {
  public class func allNodes(from file: Swift.String) -> [SceneKit.SCNNode]
}
public protocol BoxMeasurementDelegate {
  func box(length: Swift.Float, width: Swift.Float, height: Swift.Float)
  func dismiss()
}
@objc @_inheritsConvenienceInitializers public class ViewController : UIKit.UIViewController, ARKit.ARSCNViewDelegate, ARKit.ARSessionDelegate, UIKit.UIGestureRecognizerDelegate {
  public var delegate: ARModule.BoxMeasurementDelegate?
  @objc override dynamic public func viewDidLoad()
  @objc override dynamic public func viewWillAppear(_ animated: Swift.Bool)
  @objc override dynamic public func viewWillDisappear(_ animated: Swift.Bool)
  @objc override dynamic public func viewDidAppear(_ animated: Swift.Bool)
  @objc public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIKit.UIGestureRecognizer) -> Swift.Bool
  @objc public func session(_ session: ARKit.ARSession, didFailWithError error: Swift.Error)
  @objc public func sessionWasInterrupted(_ session: ARKit.ARSession)
  @objc public func sessionInterruptionEnded(_ session: ARKit.ARSession)
  @objc public func renderer(_ renderer: SceneKit.SCNSceneRenderer, willRenderScene scene: SceneKit.SCNScene, atTime time: Foundation.TimeInterval)
  @objc public func session(_ session: ARKit.ARSession, didUpdate frame: ARKit.ARFrame)
  @objc public func renderer(_ renderer: SceneKit.SCNSceneRenderer, didAdd node: SceneKit.SCNNode, for anchor: ARKit.ARAnchor)
  @objc public func renderer(_ renderer: SceneKit.SCNSceneRenderer, updateAtTime time: Foundation.TimeInterval)
  @objc deinit
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
public struct CP : Swift.Codable {
  public let cloudPoint: [ARModule.CloudPoint]
  public let type: Swift.Int
  public let platform: Swift.String
  public let dimension: ARModule.Dimensions
  public init(cloudPoint: [ARModule.CloudPoint], typ: Swift.Int, platform: Swift.String, dimension: ARModule.Dimensions)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct CloudPoint : Swift.Codable {
  public let x: CoreGraphics.CGFloat
  public let y: CoreGraphics.CGFloat
  public let z: CoreGraphics.CGFloat
  public init(x: CoreGraphics.CGFloat, y: CoreGraphics.CGFloat, z: CoreGraphics.CGFloat)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct Dimensions : Swift.Codable {
  public let height: CoreGraphics.CGFloat
  public let length: CoreGraphics.CGFloat
  public let width: CoreGraphics.CGFloat
  public init(height: CoreGraphics.CGFloat, length: CoreGraphics.CGFloat, width: CoreGraphics.CGFloat)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension UIFont {
  public static func jbs_registerFont(withFilenameString filenameString: Swift.String, bundle: Foundation.Bundle)
}
public struct Response : Swift.Codable {
  public let boxPoints: [Swift.String : ARModule.BoxPoint]?
  public let data: ARModule.DataClass?
  public let message: Swift.String?
  public let resultCode: Swift.String?
  public init(boxPoints: [Swift.String : ARModule.BoxPoint]?, data: ARModule.DataClass?, message: Swift.String?, resultCode: Swift.String?)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BoxPoint : Swift.Codable {
  public let x: Swift.Double?
  public let y: Swift.Double?
  public let z: Swift.Double?
  public init(x: Swift.Double?, y: Swift.Double?, z: Swift.Double?)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DataClass : Swift.Codable {
  public let height: Swift.Double?
  public let length: Swift.Double?
  public let width: Swift.Double?
  public init(height: Swift.Double?, length: Swift.Double?, width: Swift.Double?)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct ResponseObject : Swift.Codable {
  public let boxPoints: [ARModule.BoxPoint]?
  public let data: ARModule.DataClass?
  public let message: Swift.String?
  public let resultCode: Swift.String?
  public init(boxPoints: [ARModule.BoxPoint]?, data: ARModule.DataClass?, message: Swift.String?, resultCode: Swift.String?)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
@_hasMissingDesignatedInitializers public class Reachability {
  @objc deinit
}
public func IOU(a: CoreGraphics.CGRect, b: CoreGraphics.CGRect) -> Swift.Float
extension Array where Element : Swift.Comparable {
  public func argmax() -> (Swift.Int, Element)
}
public func sigmoid(_ x: Swift.Float) -> Swift.Float
public func softmax(_ x: [Swift.Float]) -> [Swift.Float]
public enum Result<Value> {
  case success(Value)
  case failure(Swift.Error?)
}
@objc @_inheritsConvenienceInitializers public class NetworkClass : ObjectiveC.NSObject {
  @objc deinit
  @objc override dynamic public init()
}
extension ARModule.ScaleState : Swift.Equatable {}
extension ARModule.ScaleState : Swift.Hashable {}
extension ARModule.ScaleState : Swift.RawRepresentable {}
extension ARModule.TouchState : Swift.Equatable {}
extension ARModule.TouchState : Swift.Hashable {}
extension ARModule.TouchState : Swift.RawRepresentable {}
